import 'package:castigo_app/pages/home_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          accentColor: Colors.red, primaryColor: Colors.redAccent[200]),
      title: 'Castigos',
      routes: {'home': (BuildContext context) => HomePage()},
      initialRoute: 'home',
    );
  }
}
