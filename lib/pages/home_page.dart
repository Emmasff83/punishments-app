import 'dart:math';

import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Castigos'),
      ),
      body: Center(
        child: RaisedButton(
            child: Text('Generar Castigo'),
            color: Colors.red,
            textColor: Colors.white,
            shape: StadiumBorder(),
            onPressed: () => _mostrarAlerta(context)),
      ),
    );
  }

  void _mostrarAlerta(BuildContext context) {
    final int repeticiones = (Random().nextInt(9) * 5) + 10;
    final int posicion = Random().nextInt(3);
    String pos =
        posicion == 0 ? 'Perfil' : posicion == 1 ? 'Frente' : 'Espalda';
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: Text('Te corresponde hacer'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('$repeticiones sentadillas'),
              Text('$pos '),
              //FlutterLogo(size: 100.0)
              _obtenerIcono(posicion)
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar',
                  style: TextStyle(
                    color: Colors.red,
                  )),
              onPressed: () => Navigator.of(context).pop(),
            ),
            FlatButton(
              child: Text('Ok',
                  style: TextStyle(
                    color: Colors.red,
                  )),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  Widget _obtenerIcono(int posicion) {
    String pos = posicion == 0
        ? 'assets/img/perfil.jpg'
        : posicion == 1 ? 'assets/img/frente.jpg' : 'assets/img/espalda.jpg';
    return Image(
      image: AssetImage(pos),
      height: 100.0,
      fit: BoxFit.cover,
    );
  }
}
